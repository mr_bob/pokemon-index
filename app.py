import pandas as pd

''' Read CSV into dataframe
Print Pokemon stats by ID
Take an argument from calling this 
app and return data based off Pk ID'''


def build_df(filename, debug=False):
    '''
    This function will read a CSV file and return a dataframe object
    '''
    #print(filename)
    raw_df = pd.read_csv(filename)

    if debug:
        print(raw_df)

    return raw_df

def main():
    # build a DF from the CSV
    df = build_df(filename, debug=True)
    # Lookup data from DF
    pass


if __name__ == "__main__":
    filename = 'Pokemon.csv'
  
    main()

