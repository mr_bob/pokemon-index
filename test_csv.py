# This is to test my knowledge with pandas

# Load the Pandas libraries with alias 'pd'

import pandas as pd

# Read data from file 'Pokemon.csv'
# (in the same directory that your python process is based)
# Control delimiters, rows, column names with read_csv (see later)

df = pd.read_csv("Pokemon.csv")
print(df)





# import pandas
# import sys

# def testing_poke(number):
#     pokemon = pandas.read_csv('Pokemon.csv')
#     print(pokemon)

# def main():
#     try:
#         number = sys.argv[1]
#     except IndexError:
#         print('Index out of range, no file provided.')
#         sys.exit(1)
#     testing_poke(number)

# if __name__ == '__main__':
#     main()                